import snowflake.connector
import pandas as pd


# Connectio string
conn = snowflake.connector.connect(
                user="XXXX",
                password="XXXX",
                account="protiviti.east-us-2.azure",
                warehouse="COMPUTE_WH",
                database="TEST_DB",
                schema="public"
                )

# Create cursor
cur = conn.cursor()

# Execute SQL statement
cur.execute("select current_date;")


cur.execute("use warehouse LOAD_WH")

cur.execute("CREATE OR REPLACE TABLE "
        "test_table(col1 integer, col2 string)")

cur.execute(
        "INSERT INTO test_table(col1, col2) VALUES " + 
        "    (123, 'test string1'), " + 
        "    (456, 'test string2')")


# Fetch result


print (cur.sfqid)
