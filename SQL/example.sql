CREATE OR REPLACE PROCEDURE "LOAD_EMPLOYEES_DATA"("TODAY_DATE" VARCHAR(16777216))
RETURNS VARCHAR(16777216)
LANGUAGE JAVASCRIPT
EXECUTE AS OWNER
AS '
    var sql_command = ''INSERT INTO EMPLOYEES(LOAD_TIME) VALUES(:1);''
    snowflake.execute(
        {
        sqlText: sql_command,
        binds: [TODAY_DATE]
        }
        );
  return "SUCCEEDED";
  ';
#----------------------------------------------------------

#--------------------------------------
